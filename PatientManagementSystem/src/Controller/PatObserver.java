/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 * Observer for patient - the secretaries 
 * @author suzmi
 */
public interface PatObserver {

    /**
     * updates secretary when the state of the patient changes
     */
    public void update();
}
