/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Appointment.Appointment;
import Model.Users.Doctor;
import Model.Users.Patient;
import View.AppointmentPanel;
import java.util.Date;

/**
 * To manage the appointments 
 * @author suzmi
 */
public class AppointmentController {
    Appointment app;
    Patient pat;
    AppointmentPanel view;
    
    /**
     * New appointment request created so patient needs to be notified
     * @param d
     * @param selected
     */
    public void createAppointment(Doctor d, Date selected) {
        app = new Appointment(d, selected);
        
        view.writeMessage(app.request(pat));
    }
}
