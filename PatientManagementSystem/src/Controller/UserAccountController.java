/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Users.Administrator;
import Model.UserAccount.UserAccount;
import Model.Users.Users;
import Serialiser.Serialiser;
import Model.UserAccountStrategy.AdministratorStrategy;
import Model.UserAccountStrategy.DoctorStrategy;
import Model.UserAccountStrategy.PatientStrategy;
import Model.UserAccountStrategy.SecretaryStrategy;
import View.JSignUpPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Controls the creation and removal of user accounts
 * @author suzmi
 */
public class UserAccountController implements ActionListener {
    private JSignUpPane view;     // to get details from inputs of user
    private Users model;          // to create/log in user
    private String password; 
    private String firstName;
    private String lastName;
    private String address;
    private String postcode;
    private ArrayList<Users> registerUsers;
    private ArrayList<Users> loggedUsers;
    Serialiser s1 = new Serialiser("registered_file.txt");  // to write registerUsers list
    Serialiser s2 = new Serialiser("logged_file.ser"); // to write loggedUsers list
    
    /**
     * Initialises lists to hold registered and logged in users to compare. 
     */
    public UserAccountController() {
        registerUsers = new ArrayList<Users>();      
        loggedUsers = new ArrayList<Users>();
    }
    
    /**
     * Adds the created user to the registerUsers list
     * Checks against loggedUsers if user already exists
     */
    public void registerUser()
    {   

    }
     
    /**
     * Logins in user using their username - unique ID and their associated password
     * @param username
     * @param password
     */
    public void logIn(String username, String password)
    {
     
    }

    @Override
    public void actionPerformed(ActionEvent e) {    
     
    }
    
    
}
