/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Users.Observable;

/**
 * Used to control the user observer pattern 
 * @author suzmi
 */
public class UserController {
    private Observable model;
    
    /**
     * Sets the observable as the model and adds observers 
     * @param model
     */
    public void setModel(Observable model){        
        this.model = model;       
        model.registerObserver((Observer) this);
    }
    
}
