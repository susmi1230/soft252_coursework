/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 * Administrators as observers. Implementation provided in Administrator class
 * @author suzmi
 */
public interface Observer {

    /**
     * updates administrator when the other accounts(doctors and secretaries) change state
     */
    public void update();
}
