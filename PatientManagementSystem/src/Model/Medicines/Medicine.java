/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Medicines;

import java.io.Serializable;

/**
 *
 * @author suzmi
 */
public class Medicine implements Serializable {
    private String name; // name of medicine
    private String quantity; // number of medicine left
    private String category; // type of medicine e.g. antibiotic

    /**
     * Gets medicine name
     * @return name of medicine
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the medicine
     * First validates that the name isn't an empty string
     * @param name name to set as medicine name
     */
    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        } // End if    
    } // End setName()

    /**
     * Gets the quantity of medicine 
     * @return quantity of medicine in stock
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the medicine quantity
     * @param quantity quantity of medicine to set as
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets the medicine category
     * @return gets the type of medicine - category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the category of the medicine
     * @param category the category to set as 
     */
    public void setCategory(String category) {
        this.category = category;
    }
    
} // End class Medicine()
