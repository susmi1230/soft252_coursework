package Model.Users;

import Model.UserAccount.DoctorAccount;
import java.util.ArrayList;
import Controller.Observer;
import Others.RandomNumber;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A type of User
 * 
 * @author suzmi
 */
public class Doctor extends Users implements Observable {
    // Instance variables
    private DoctorAccount account; // aggregation
    private ArrayList<Observer> observers = new ArrayList<>(); // Stores secretaries

    /**
     * Initialises Doctor 
     * Inherited from parent class - Users
     * @param forename Doctor's first name
     * @param surname Doctor's last name
     * @param address Doctor's address
     * @param postcode Doctor's postcode
     */
    public Doctor(String forename, String surname, String address, String postcode) {
        super(forename, surname, address, postcode);
    }
    
    /**
     * Gets Doctor's account
     * @return 
     */
    public DoctorAccount getAccount() {
        return account;
    }

    /**
     * Sets Doctor's account
     * @param account An account 
     */
    public void setAccount(DoctorAccount account) {
        this.account = account;
    }
    
    /**
     * Adds Observer - Administrator
     * @param observer Administrator 
     */
    @Override
    public void registerObserver(Observer observer) {   
        
        observers.add(observer);
        
    } // End registerObserver()

    /**
     * Removes observer - Administrator
     * Finds index by comparing observer to list of observers
     * Removes index if observer found i.e. removes observer 
     * @param observer Administrator
     */
    @Override
    public void removeObserver(Observer observer) { 
        
        int index = observers.indexOf(observer);
        
        if (index > 0) {                
            observers.remove(index);            
        } // End if
        
    } // End removeObserver()

    /**
     * Notifies all administrators 
     */
    @Override
    public void notifyAllObserver() {
        
    } // End notifyAllObserver()
   
} // End Class Doctor()
