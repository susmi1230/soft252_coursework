package Model.Users;


import Model.Users.PatObservable;
import Controller.PatObserver;
import java.util.Date;
import Model.UserAccount.PatientAccount;
import java.util.ArrayList;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Type of User
 * 
 * @author suzmi
 */
public class Patient extends Users implements PatObservable {
    // Instance variables
    private String gender;
    private Date dob;       // Date of birth
    private int age;    
    private PatientAccount account; // account of patient
    private ArrayList<PatObserver> observers = new ArrayList<>(); // List of secretaries
    
    /**
     * Gets patient account
     * @return
     */
    public PatientAccount getAccount() {
        return account;
    }

    /**
     * Sets an account to patient
     * @param account
     */
    public void setAccount(PatientAccount account) {
        this.account = account;
    }
    
    /**
     * Initialises Patient
     * Inherited from parent class - Users
     * @param forename
     * @param surname
     * @param address
     * @param postcode
     */
    public Patient(String forename, String surname, String address, String postcode) {
        super(forename, surname, address, postcode);
    }

    /**
     * Gets patient's gender
     * @return
     */
    public String getGender() { 
        return gender;
    } 

    /**
     * Sets patient's gender
     * First Validates input is not an empty string
     * @param gender
     */
    public void setGender(String gender) {               
        if (!gender.isEmpty()) {                    
            this.gender = gender;       
        } else {
            gender = "not set";
        } // End if  
    } // End setGender()

    /**
     * Gets date of birth of patient
     * @return
     */
    public Date getDob() {
        return dob;
    } 

    /**
     * Sets date of birth of patient
     * First validates it is not null
     * @param dob
     */
    public void setDob(Date dob) {
        if (dob != null) {
            this.dob = dob;
        } // End if  
    } // End setDob()

    /**
     * Gets patient's age
     * @return
     */
    public Integer getAge() {
        return age;    
    } 

    /**
     * Sets patient's age
     * Checks age is not less than 0 
     * @param age
     */
    public void setAge(int age) {
        if (age >= 0) {
            this.age = age; 
        } else {
            age = 0;
        } // End if     
    } // End setAge()

    /**
     * Registers observer - secretary
     * Add secretary to arrayList
     * @param observer 
     */
    public void registerObserver(PatObserver observer) {   
        observers.add(observer);        
    } // End registerObserver()

    /**
     * Removes observer - secretary
     * @param observer 
     */
    public void removeObserver(PatObserver observer) {
        int index = observers.indexOf(observer); // Get index of observer in arrayList
        
        // if index is not less than 0
        // remove observer at the index
        if (index > 0) {                  
            observers.remove(index);       
        } // End if     
    } // End removeObserver()
 
    /**
     * Notifies all observers to update
     */
    @Override
    public void notifyAllObservers() {          
    } // End notifyObserver()
     
} // End Class Patient
