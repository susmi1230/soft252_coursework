package Model.Users;

import Controller.Observer;
import Model.UserAccount.AdministratorAccount;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A type of User
 * 
 * @author suzmi
 */
public class Administrator extends Users implements Observer {
    /**
     * Aggregation - Administrator has an AdministratorAccount
     * Administrator is another class
     */
    private AdministratorAccount account; 

    /**
     * Initialises a new administrator
     * Uses code from Users class - parent class
     * @param forename Administrator's first name
     * @param surname  Administrator's last name
     * @param address  Administrator's address
     * @param postcode Administrator's postcode
     */
    public Administrator(String forename, String surname, String address, String postcode) {
        super(forename, surname, address, postcode);    
    }
    
    /**
     * 
     */
    @Override
    public void update() {
        
    } // End update()


  
}
