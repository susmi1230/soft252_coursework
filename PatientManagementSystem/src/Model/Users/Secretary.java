package Model.Users;


import Model.Users.Observable;
import Model.UserAccount.SecretaryAccount;
import java.util.ArrayList;
import Controller.Observer;
import Controller.PatObserver;
import View.JSignUpPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author suzmi
 */
public class Secretary extends Users implements PatObserver, Observable {
    JSignUpPane view;
    // Instance variables
    private SecretaryAccount account; // Aggregation - Account of secretary
    private ArrayList<Observer> observers = new ArrayList<Observer>(); // Holds list of administrators

    /**
     * Initialises Secretary 
     * @param forename Secretary's first name
     * @param surname Secretary's last name
     * @param address Secretary's address
     * @param postcode Secretary's postcode
     */
    public Secretary(String forename, String surname, String address, String postcode) {
        super(forename, surname, address, postcode);      
    }

    /**
     * Gets secretary's account
     * @return
     */
    public SecretaryAccount getAccount() {    
        return account;
    }

    /**
     * Sets account to secretary's account
     * @param account account to set
     */
    public void setAccount(SecretaryAccount account) {
        this.account = account;
    }
    
    /**
     * Adds observer
     * @param observer Administrator
     */ 
    @Override
    public void registerObserver(Observer observer) {      
        observers.add(observer);        
    } // registerObserver()

    /**
     * Removes observer
     * @param observer Administrator
     */
    @Override
    public void removeObserver(Observer observer) {     
        int index = observers.indexOf(observer); // Get index of observer
        
        // If index is more than 0
        // then remove observer at index
        if (index > 0) {                 
            observers.remove(index);           
        } // End if   
    } // End removeObserver()
    
    /**
     * Notifies all administrators to update
     */
    @Override
    public void notifyAllObserver() {
        
    } // End notifyAllObserver()
    
    // Updates on change of observable - patient
    @Override
    public void update() {

    } // End update()
  
} // End Class Secretary()
