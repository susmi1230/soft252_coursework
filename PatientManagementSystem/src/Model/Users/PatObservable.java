/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Users;
import Controller.PatObserver;

/**
 * Observable is a patient
 * 
 * @author suzmi
 */
public interface PatObservable {
    
     /**
     * Register observer - Secretary.
     * @param observer
     */
    public void registerObserver(PatObserver observer);

    /**
     * Remove observer - Secretary.
     * @param observer
     */
    public void removeObserver(PatObserver observer);

    /**
     * notify all observers - Secretary. 
     */
    public void notifyAllObservers();

}
