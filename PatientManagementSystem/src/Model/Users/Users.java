package Model.Users;

import Controller.Observer;
import Model.UserAccount.UserAccount;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Parent class - types of users inherit from this class 
 * 
 * @author suzmi
 */
public abstract class Users {
    //Instance variables
    /**
     * First name of user
     */
    protected String forename;   

    /**
     * User's last name
     */
    protected String surname;   
    
    /**
     * Includes house name/number, street name and city
     */
    protected String address;    

    /**
     * corresponding postcode to user's address 
     * does not check that postcode matches address 
     */
    protected String postcode;   

    /**
     * optional mobile number to contact user
     * can be added later on
     */
    protected String mobile;     

    /**
     * optional telephone number
     * can be added later on
     */
    protected String tel;  

    /**
     * User's online account 
     * UserAccount is a class and property of User - aggregation
     */
    protected UserAccount account;
    
    /**
     * Initialises Users - inherited by users types (children)
     * @param forename User's first Name
     * @param surname User's last Name
     * @param address User's address
     * @param postcode User's postcode
     */
    public Users(String forename, String surname, String address, String postcode) {
        this.forename = forename;       
        this.surname = surname;
        this.address = address;
        this.postcode = postcode;
    }
            
    /**
     * Gets the first Name of User
     * @return User's first name
     */
    public String getForename() {
        return forename;
    }
    
    /**
     * Sets User's first name
     * Checks string isn't empty
     * @param forename 
     */
    public void setForename(String forename) {
        if (!forename.isEmpty()) {
            this.forename = forename;  
        } else {
            this.forename = "not set";
        } // End if
    } // End setForename()

    /**
     * Gets User's last name
     * @return User's last name
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets User's last name
     * Validates last name is not empty
     * @param surname
     */
    public void setSurname(String surname) {
        if (!surname.isEmpty()) {
           this.surname = surname; 
        } else {
            this.surname = "not set";
        } // End if
    } // End setSurname()
       
    /**
     * Gets User's address
     * @return User's address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets User's address
     * @param address
     */
    public void setAddress(String address) {
        if (!address.isEmpty()) {
           this.address = address;
        } else {
            this.address = "not set";
        }     
    } // End setAddress()
    
    /**
     * Gets user's postcode
     * @return  User's postcode
     */
    public String getPostcode() {
        return postcode;
    }
    
    /**
     * Takes in postcode to set and validates
     * Checks postcode in right pattern and not empty
     * before setting as postcode
     * @param postcode postcode to validate
     */
    public void setPostcode(String postcode) {
        // To check is valid UK postcode 
        String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";      
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(postcode);
        
        if (!postcode.isEmpty()) {           
            if (matcher.matches()) {               
                this.postcode = postcode;             
            } // End if         
        } else {
            this.postcode = "not set";
        }     
    } // End setPostcode()

    /**
     * Gets User's mobile number
     * @return User's mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets user's mobile number
     * @param mobile
     */
    public void setMobile(String mobile) {
        if (!mobile.isEmpty()) {
           this.mobile = mobile;
        } else {
            this.mobile = "not set";
        }  
    } // End setMobile()
    
    /**
     * Gets user's telephone number
     * @return User's telephone number
     */
    public String getTel() {
        return tel;
    }
    
    /**
     * Sets user's telephone number
     * Validates number is not empty first
     * @param tel
     */
    public void setTel(String tel) {
        if (!tel.isEmpty()) {
           this.tel = tel;
        } else {
            this.tel = "not set";
        }     
    } // End setTel()
    
} // End Class Users()
