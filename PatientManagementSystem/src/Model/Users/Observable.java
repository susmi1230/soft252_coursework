/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Users;

import Controller.Observer;

/**
 * Observables - Doctors and secretaries 
 * 
 * @author suzmi
 */
public interface Observable {
    /**
     * Register observer - Administrator.
     * @param observer
     */
    public void registerObserver(Observer observer);

    /**
     * Remove observer - Administrator.
     * @param observer
     */
    public void removeObserver(Observer observer);

    /**
     * Notify all observers - Administrators. 
     */
    public void notifyAllObserver();
}
