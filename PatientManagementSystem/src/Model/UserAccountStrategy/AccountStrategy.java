/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccountStrategy;

/**
 * Interface for creating accounts strategy
 * @author suzmi
 */
public interface AccountStrategy {

    /**
     * Implementations provided by implemented classes
     */
    public void Request();
    
} // End class AccountStrategy()
