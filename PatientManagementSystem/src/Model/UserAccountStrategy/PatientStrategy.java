/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccountStrategy;

import View.SecretaryPanel;

/**
 *
 * @author suzmi
 */
public class PatientStrategy implements AccountStrategy {
    SecretaryPanel secView;
    
    /**
     * Sends message to TextArea of SecretaryPanel to append
     */
    public PatientStrategy() {
        secView = new SecretaryPanel();
        
        Request();
    }
    
    /**
     * Writes a message that a doctor has been created. Will be appended 
     * to the notifications TextArea of the secretary panel.  
     */
    public void Request() {     
        String message = "There is a new patient. \n Check requests.";
        
        secView.writeMessage(message);      
    }
    
}
