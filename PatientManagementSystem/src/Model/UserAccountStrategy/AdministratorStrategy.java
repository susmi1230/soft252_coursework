/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccountStrategy;


import Model.Users.Administrator;
import Model.Users.Observable;
import View.AdministratorPanel;
import View.JSignUpPane;
import java.util.ArrayList;

/**
 *
 * @author suzmi
 */
public class AdministratorStrategy implements AccountStrategy {
    private AdministratorPanel view; // To display notificatiob
    private Observable model; // 
    private ArrayList<Administrator> admins = new ArrayList<>();
    
    /**
     * Sends message to TextArea of AdministratorPanel to append
     */
    public AdministratorStrategy() {
        view = new AdministratorPanel();
        
        Request();
        
    }
    
    /**
     * Writes a message that an administrator has been created. Will be appended 
     * to the notifications TextArea of the administrator panel.  
     */
    public void Request() {
        String message = "There is a new administrator. \n Check requests.";
        
        view.writeMessage(message);        
    }
    
//     public void setModel(Observable model){
//        this.model = model;
//        model.registerObserver(this);
//    }
    
} // End Class AdministratorStrategy()
