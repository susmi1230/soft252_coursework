/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccountStrategy;

import View.AdministratorPanel;
import View.SecretaryPanel;

/**
 *
 * @author suzmi
 */
public class SecretaryStrategy implements AccountStrategy {
    AdministratorPanel secView;
    
    /**
     * Sends message to TextArea of AdministratorPanel to append
     */
    public SecretaryStrategy() {
        secView = new AdministratorPanel();
        
        Request();
    }
    
    /**
     * Writes a message that a doctor has been created. Will be appended 
     * to the notifications TextArea of the administrator panel.  
     */
    public void Request() {     
        String message = "There is a new secretary. \n Check requests.";
        
        secView.writeMessage(message);    
    }
}
