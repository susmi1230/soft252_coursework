/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Ratings;

import java.io.Serializable;

/**
 * Ratings from user to be displayed in ratings panel
 * @author suzmi
 */
public class Ratings implements Serializable {
    String doctorName;
    String rating;
    String feedback;
    
    /**
     * Initialises rating
     * @param doctorName name of doctor to rate
     * @param rating number of stars out of 5
     * @param feedback feedback from textArea
     */
    public Ratings(String doctorName, String rating, String feedback) {
        this.doctorName = doctorName;
        this.rating = rating;
        this.feedback = feedback;
    }

    /**
     * Gets a message to print in ratings panel textArea 
     * @return message to be displayed
     */
    public String getRating() {
        String message;
        
        message = "Doctor's name: " + doctorName + ", Rating: " + rating 
                + ", Feedback: "+ feedback;
        
        return message;
    }
}
