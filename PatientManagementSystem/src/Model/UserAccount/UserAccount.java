package Model.UserAccount;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author suzmi
 */
public abstract class UserAccount implements Serializable {

    /**
     * unique ID used to log in
     */
    protected String uniqueID;

    /**
     * password used to log in 
     */
    protected String password;

    /**
     * Gets unique ID 
     * @return uniqueID
     */
    public String getUniqueID() {
        return uniqueID;
    }

    /**
     * Gets password
     * @return password
     */
    public String getPassword() {
        return password;
    } 

    /**
     * Sets password
     * Checks length is more than 8 then
     * Checks there is a number
     * If both fulfilled, password set
     * @param password password to set
     */
    public void setPassword(String password) {
        // Check is valid password
        int passwordLength = 8;
        int numCount = 0;
        
        if (password.length() >= passwordLength) {           
            for (int i = 0; i < password.length(); i++) { 
                char letter = password.charAt(i);  
  
                if (letter >= '0' && letter <= '9' )
                {
                    this.password = password;                    
                } // End if                
            } // End for
        } // End if 
        else {
            System.out.println("Password does not meet requirements");
        }
    } // End setPassword()
    
} // End Class UserAccount
