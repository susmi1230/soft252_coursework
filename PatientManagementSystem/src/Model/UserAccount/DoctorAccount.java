/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccount;

import Others.RandomNumber;
import java.io.Serializable;

/**
 *
 * @author suzmi
 */
public class DoctorAccount extends UserAccount implements Serializable {
    // Instance variables
    private RandomNumber r = new RandomNumber(); // to generate unique ID

    /**
     * Account of doctor
     * @param uniqueID temporary unique ID, gets changed
     * @param password password to set
     */
    public DoctorAccount(String uniqueID, String password) {
        uniqueID = "D" + r.Generate();
        
        this.uniqueID = uniqueID;
        this.password = password;
    }  
    
} // End class DoctorAccount()
