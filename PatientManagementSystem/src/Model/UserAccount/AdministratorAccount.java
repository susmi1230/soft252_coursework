/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccount;
import Others.RandomNumber;
import java.io.Serializable;

/**
 * Account of administrator
 * Inherits from UserAccount
 * 
 * @author suzmi
 */
public class AdministratorAccount extends UserAccount implements Serializable {
    // Instance variables
    private RandomNumber r = new RandomNumber(); // Used to generate uniqueID in constructor    

    /**
     * Initialises administrator's account
     * @param uniqueID temporary unique ID, gets changed 
     * @param password password to set 
     */
    public AdministratorAccount(String uniqueID, String password) {
        uniqueID = "A" + r.Generate();    
        this.uniqueID = uniqueID;   
        this.password = password;
    }
   
} // End Class AdministratorAccount()
