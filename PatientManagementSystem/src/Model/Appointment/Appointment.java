/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Appointment;

import Model.Users.Doctor;
import Model.Users.Patient;
import java.util.Date;

/**
 *
 * @author suzmi
 */
public class Appointment {
    private Doctor doctor; // Doctor assigned for appointment
    private Date selectedDate; // Date selected for appointment

    /**
     * Gets doctors details who has the consultation
     * @return doctor selected for appointment
     */
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     * Sets the doctor for the appointment
     * @param doctor Doctor to set
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     * Gets the date for the appointment
     * @return selected date for appointment
     */
    public Date getSelectedDate() {
        return selectedDate;
    }

    /**
     * Sets the date for the appointment
     * @param selectedDate date selected for appointment
     */
    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    } 
    
    /**
     * Creates appointment with doctor at selected date
     * @param doctor
     * @param selectedDate
     */
    public Appointment(Doctor doctor, Date selectedDate) {
        this.doctor = doctor;
        this.selectedDate = selectedDate;       
    }
    
    /**
     * Creates notification of appointment to send
     * @param p Patient who has appointment
     * @return message to send 
     */
    public String request(Patient p) {
        String message = p.getForename() + " " + p.getSurname() + "has a new appointment request from Dr " + doctor + " for " + selectedDate.toString();
        
        return message; 
    } // End request()

} // End Class Appointment()
