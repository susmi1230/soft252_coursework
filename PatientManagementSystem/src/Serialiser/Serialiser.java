/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serialiser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * To read and write objects such as arrayLists
 * Used to save object to a file to used after shutdown of application.
 * @author suzmi
 */
public class Serialiser {
    private String fileName; // name of file
    
    /**
     * Choose file to read to and write from
     * @param filename name of file 
     */
    public Serialiser(String filename){
        this.fileName = filename;
    }
    
    /**
     * Sets name of file
     * @param filename name of file to set as
     */
    public void setFileName(String filename){
        this.fileName = filename;
    }
    
    /**
     * Gets file name
     * @return name of file
     */
    public String getFileName(){
        return fileName;
    }
    
    /**
     * To read objects from a file
     * Reads bites from fileName and converts byte stream to
     * an object 
     * @return object from file
     */
    public Serializable readObject(){
        Serializable loadedObject = null;
        try {
         FileInputStream fileIn = new FileInputStream(fileName);
         ObjectInputStream in = new ObjectInputStream(fileIn);
         loadedObject = (Serializable) in.readObject();
         in.close();
         fileIn.close();
         System.out.println("Success! Data loaded from: "+ this.fileName);
        } catch (IOException i) {
            System.out.println("File not found.");
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
        }
        return loadedObject;
    } // End readObject()
    
    /**
     * Write object to file by converting the objects to a
     * stream of bytes using FileOutputStream. FileOutputStream
     * is used to write bytes to the file. 
     * @param object
     * @return
     */
    public boolean writeObject(Serializable object){
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
            System.out.println("Success! The serialized data is saved in: " + fileName);
            return true;
         } catch (IOException i) {            
             System.out.println("Did not load");            
             i.printStackTrace();           
             return false;       
         }
    } // End writeObject()
    
} // End class Serialiser()
