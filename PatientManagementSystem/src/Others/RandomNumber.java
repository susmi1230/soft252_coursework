/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Others;

import java.util.ArrayList;
import java.util.Random;

/**
 * To generate random number for creating unique IDs
 * @author suzmi
 */
public class RandomNumber {
    
    /**
     * Generates a random number.
     * A random number generator is intialised as well as an ArrayList 
     * holding previously generated random numbers. The number generated
     * will be four digits long. A new number is generated until the number
     * is not found in rList - the ArrayList. This new number is added to
     * rList before being returned. 
     * @return the new random number
     */
    public int Generate() { 
    Random r = new Random();  
    ArrayList<Integer> rList = new ArrayList<>(); 
    int numb = r.nextInt(8999) + 1000;            // Generate four digit number       
            
    while (rList.contains(numb))        
    {          
        numb = r.nextInt(8999) + 1000;         
    } //End while
       
    rList.add(numb);
       
    return numb;   
    } // End Generate()
   
} // End Class RandomNumber
