/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;


import Model.UserAccount.AdministratorAccount;
import Model.UserAccount.DoctorAccount;
import Model.UserAccount.PatientAccount;
import Model.UserAccount.SecretaryAccount;
import Model.UserAccount.UserAccount;
import Serialiser.Serialiser;
import View.JLogInPane;
import java.util.ArrayList;

/**
 *
 * @author suzmi
 */
public class GUISimulator {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<UserAccount> users = new ArrayList<>();
        
        //Instantiate models
        AdministratorAccount a1 = new AdministratorAccount("temp", "password123");  
        users.add(a1);
        
        DoctorAccount d1 = new DoctorAccount("temp", "password123");
        users.add(d1);
        
        DoctorAccount d2 = new DoctorAccount("temp", "password123");
        users.add(d2);
        
        DoctorAccount d3 = new DoctorAccount("temp", "password123");
        users.add(d3);
        
        PatientAccount p1 = new PatientAccount("temp", "password123");
        users.add(p1);
        
        PatientAccount p2 = new PatientAccount("temp", "password123");
        users.add(p2);
        
        PatientAccount p3 = new PatientAccount("temp", "password123");
        users.add(p3);
        
        SecretaryAccount s1 = new SecretaryAccount("temp", "password123");
        users.add(s1);
        
        for (int i = 0; i < users.size(); i++) {
            System.out.print(users.get(i).getUniqueID() + "\n");
        }
        
        Serialiser s = new Serialiser("users.txt");
        
        s.writeObject(users);
       
        // Instantiate view
        JLogInPane view = new JLogInPane();
        view.setVisible(true);
        
        // Instantiate controllers
        
    }
    
}
