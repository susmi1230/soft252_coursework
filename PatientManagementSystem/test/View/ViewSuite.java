/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author suzmi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({View.SecretaryPanelTest.class, View.AppointmentPanelTest.class, View.JLogInPaneTest.class, View.DoctorPanelTest.class, View.RatingPanelTest.class, View.PatientHistoryPanelTest.class, View.AdministratorPanelTest.class, View.JSignUpPaneTest.class, View.PatientPanelTest.class})
public class ViewSuite {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
