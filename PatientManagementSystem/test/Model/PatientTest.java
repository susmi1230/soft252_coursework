/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.UserAccount.PatientAccount;
import Model.Users.Patient;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author suzmi
 */
public class PatientTest {
    Patient p = new Patient("Anna", "Harris", "13", "pl");

    @Test
    public void testGetAccount() {
//         assertEquals(p, p.getAccount()); // fails as account not set; works!
    }

    @Test
    public void testSetAccount() {
        PatientAccount patAcc = new PatientAccount("D2334", "password12");
        
        p.setAccount(patAcc);
        
        assertEquals(patAcc, p.getAccount()); // passes as accounts match and details are fine
    }

    @Test
    public void testSetGender() {
        p.setGender("");
        
//      assertEquals("", p.getGender()); // fails as gender will set to "not set"
        
        p.setGender("female");
        
        assertEquals("female", p.getGender()); // passes as equal strings
    }

    @Test
    public void testSetDob() {    
        Date d = new Date();
        
        p.setDob(d);
        
//      assertEquals("", p.getDob()); // fails as compared to empty string 
        
        p.setDob(null);
        
//      assertEquals(null, p.getDob()); // fails as does not change Dob to null
        
    }
    
}
