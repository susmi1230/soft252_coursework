/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.UserAccount.DoctorAccount;
import Model.Users.Doctor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author suzmi
 */
public class DoctorTest {
    Doctor d1 = new Doctor("Anna", "Harris", "13", "pl");
    Doctor d2 = new Doctor("awe", "", "13", "pl");
        
    public DoctorTest() {
        d2.setForename("");
        
        // assertEquals("Anna", d1.getAddress()); // Should fail; it works!
//      assertEquals("awe", d2.getForename()); // should fail as empty string 
                                               // tried to be set so name changed
                                               // to "not set" ; it works!
    }
    
    @Test
    public void testGetAccount() {
//        assertEquals(d1, d1.getAccount()); // fails as account not set; works!
//      assertEquals(d2, d2.getAccount()); // fails as account not set; works!
    }

    @Test
    public void testSetAccount() {
        DoctorAccount docAcc = new DoctorAccount("D2334", "password12");
        
        d1.setAccount(docAcc);
        
        assertEquals(docAcc, d1.getAccount()); // passes as accounts match and details are fine
    }
    
}
