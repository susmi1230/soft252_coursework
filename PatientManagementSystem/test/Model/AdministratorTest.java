/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.Users.Administrator;
import static org.junit.Assert.*;

/**
 *
 * @author suzmi
 */
public class AdministratorTest {
    
    public AdministratorTest() {
        Administrator a = new Administrator("Anna", "Harris", "13", "pl");
        
        a.setSurname("");
        a.setAddress("A");
        a.setPostcode("pl");
        
        assertEquals("Anna", a.getForename()); // Should work; it works!
//      assertEquals("", a.getSurname()); // should fail; works!
        assertEquals("A", a.getAddress()); // should pass; works!
        assertEquals("pl", a.getPostcode()); // should pass; works!
    }

}
