/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.UserAccount.SecretaryAccount;
import Model.Users.Secretary;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author suzmi
 */
public class SecretaryTest {
    Secretary s = new Secretary("Andy", "Harris", "13", "pl");
    
    public SecretaryTest() {
         s.setForename("");
         
        // assertEquals("", s.getForename()); // should fail as sets as "not set" 
                                            // if empty string; it works
    }
    
    /*
    Tests setAccount() as well as getAccount()
    */
    @Test
    public void testGetAccount() {
        SecretaryAccount secAcc = new SecretaryAccount("D2334", "password12");
        
        s.setAccount(secAcc);
        
        assertEquals(secAcc, s.getAccount()); // passes as accounts match and details are fine
    }
    
}
