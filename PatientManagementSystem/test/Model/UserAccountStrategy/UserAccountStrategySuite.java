/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.UserAccountStrategy;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author suzmi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Model.UserAccountStrategy.DoctorStrategyTest.class, Model.UserAccountStrategy.SecretaryStrategyTest.class, Model.UserAccountStrategy.AdministratorStrategyTest.class, Model.UserAccountStrategy.PatientStrategyTest.class})
public class UserAccountStrategySuite {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
