/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Ratings.Ratings;
import Model.Users.Doctor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests getRating() method for:
 * successful printing of message
 * 
 * @author suzmi
 */
public class RatingsTest {

    @Test
    public void testGetRating() {
        Doctor d = new Doctor("Anna", "Harris", "13", "pl");
        String message = "Doctor's name: " + d.getForename() + ", Rating: 5" 
                + ", Feedback: Very good";
        Ratings r = new Ratings(d.getForename(), "5", "Very good");
        
        assertEquals(message, r.getRating()); // should succeed; it works!
        
    }
    
}
