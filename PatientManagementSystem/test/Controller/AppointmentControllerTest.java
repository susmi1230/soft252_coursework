/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Appointment.Appointment;
import Model.UserAccount.DoctorAccount;
import Model.Users.Doctor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests createAppointment() method for:
 * Changes in dates
 * Validates values are retrieved successfully using getter and setter methods
 * @author suzmi
 */
public class AppointmentControllerTest {
    
    /*
    Tests for matching date and doctor which is passed to appointment argument 
    */
    @Test
    public void testCreateAppointment() throws ParseException {
        
        DoctorAccount acc = new DoctorAccount("D4536", "password1");    
        Doctor d = new Doctor("Sandra", "Wilson", "186 evergeen terrace", "pl23wd");
        
        d.setAccount(acc);
        
        String strDateFormat = "dd-MMM-yyyy"; //Date format Specified
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat); //Date format string is passed as an argument to the Date format object
        
        Date selected = sdf.parse("12-JAN-2012"); // Date of appointment 
        Date rightDate = sdf.parse("12-JAN-2012"); // Date that matches selected
        Date wrongDate = sdf.parse("2014-02-14"); // Date that doesn't match selected
        
        Appointment app = new Appointment(d, selected);
        
        assertEquals(d ,app.getDoctor()); // should be successful; it works
       // assertEquals(wrongDate, selected); // Should fail; works!
        assertEquals(rightDate, selected); // should succeed; Works!
    }
    
}
